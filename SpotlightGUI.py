# -*- coding: utf-8 -*-
import os
import shutil
import PIL.Image

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(300, 200)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lb_title = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setWeight(50)

        # Title label
        self.lb_title.setFont(font)
        self.lb_title.setAlignment(QtCore.Qt.AlignCenter)
        self.lb_title.setObjectName("lb_title")
        self.verticalLayout.addWidget(self.lb_title)

        # User label for the line edit field
        self.lb_user = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lb_user.setFont(font)
        self.lb_user.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.lb_user.setFrameShadow(QtWidgets.QFrame.Plain)
        self.lb_user.setLineWidth(1)
        self.lb_user.setObjectName("lb_user")
        self.verticalLayout.addWidget(self.lb_user)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        # Line edit for entering username
        self.le_user = QtWidgets.QLineEdit(self.centralwidget)
        self.le_user.setObjectName("le_user")
        self.horizontalLayout.addWidget(self.le_user)
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Folder label for the line edit
        self.lb_folder = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lb_folder.setFont(font)
        self.lb_folder.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.lb_folder.setObjectName("lb_folder")
        self.verticalLayout.addWidget(self.lb_folder)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")

        # Line edit for entering the folder
        self.le_folder = QtWidgets.QLineEdit(self.centralwidget)
        self.le_folder.setObjectName("le_folder")
        self.le_folder.setText("C:/Users/" + os.getenv("username") + "/Desktop/SpotlightImages/")
        self.horizontalLayout_2.addWidget(self.le_folder)

        # Button to select the folder with a dialog
        self.pb_folder = QtWidgets.QPushButton(self.centralwidget)
        self.pb_folder.setObjectName("pb_folder")
        self.horizontalLayout_2.addWidget(self.pb_folder)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Button to get the spotlight images
        self.pb_start = QtWidgets.QPushButton(self.centralwidget)
        self.pb_start.setObjectName("pb_start")
        self.verticalLayout.addWidget(self.pb_start)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate

        # Set the text, tooltip or title of the widgets
        MainWindow.setWindowTitle(_translate("MainWindow", "Spotlight Grabber"))
        self.lb_title.setText(_translate("MainWindow", "Spotlight Grabber"))
        self.lb_user.setText(_translate("MainWindow", "User"))
        self.le_user.setText(os.getenv('username'))
        self.le_user.setToolTip(_translate("MainWindow", "The user of which the spotlight images will be retrieved"))
        self.lb_folder.setText(_translate("MainWindow", "Path"))
        self.le_folder.setToolTip(_translate("MainWindow", "The path where the spotlight images will be saved.\nIf not specified the images will be saved in 'SpotlightImages' on the desktop"))
        self.pb_folder.setText(_translate("MainWindow", "Choose folder"))
        self.pb_folder.setToolTip(_translate("MainWindow", "Choose the folder where the spotlight images should be saved"))
        self.pb_start.setText(_translate("MainWindow", "Start"))
        self.pb_start.setToolTip(_translate("MainWindow", "Gets the spotlight images and saves them in the specified folder"))

        # Connetct the methods to the buttons (choose a folder, get the images)
        self.pb_folder.clicked.connect(self.setFolder)
        self.pb_start.clicked.connect(self.getImages)
        self.le_user.returnPressed.connect(self.getImages)
        self.le_folder.returnPressed.connect(self.getImages)

        # Set the focus to the start button
        self.pb_start.setFocus()

    def setFolder(self):
        """Create a dialog to choose a folder on where to save the images"""
        dialog = QtWidgets.QFileDialog()
        dialog.setFileMode(QtWidgets.QFileDialog.Directory)
        dialog.setOption(QtWidgets.QFileDialog.ShowDirsOnly)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            folder = dialog.directory().absolutePath()
            self.le_folder.setText(folder)

    def getImages(self):
        """Gets the splotlight images"""
        
        # Define a message box for showing information on the progress
        msg = QtWidgets.QMessageBox()

        # Get the current user
        user = self.le_user.text()
        if user == "":
            # If no user is specified use the current user
            user = os.getenv("username")

        src = "C:/Users/" + user + "/AppData/Local/Packages/Microsoft.Windows.ContentDeliveryManager_cw5n1h2txyewy/LocalState/Assets/"

        dst = self.le_folder.text()

        if dst == "":
            """If no folder is specified create a folder on the desktop where the images are saved"""
            dst = "C:/Users/" + user + "/Desktop/SpotlightImages/"

        if not dst.endswith("/"):
            dst = dst + "/"

        # Check if the spotlight folder is correct for the specified user
        if os.path.exists(src):
            # Create the folder if it does not exist
            if not os.path.exists(dst):
                os.makedirs(dst)

            # Get the amount of files in the destination folder to calculate the copied files
            numFilesDst = len(os.listdir(dst))

            # Go to the path of the Spotlight images
            os.chdir(src)

            # Get all files in the source directory
            asset_files = os.listdir(src)

            # Starting filenumber
            i = 1

            # Counter for excluded files
            err_i = 0

            # Rename each file and save it to the destination folder
            for file in asset_files:

                try:
                    # Get the dimension of the image
                    with PIL.Image.open(file) as img:
                        width, height = img.size

                    # Copy only the pictures in landscape format
                    if height >= width:
                        continue
                except:
                    err_i = err_i + 1
                    continue

                # Get all files in the destination directory to prevent overwriting
                directory_files = os.listdir(dst)

                # Make sure no existing files are overwritten by increasing the file index
                while("Image" + str(i) + ".jpg" in directory_files):
                    i = i + 1

                # Copy the file and rename it with the right file-extension
                shutil.copy2(src + file, dst + "Image" + str(i) + ".jpg")

            # Show a success message and where the images were saved
            msg.setText(str(i - numFilesDst) + " images were saved in:\n" + dst)
            msg.setWindowTitle("Done")
            msg.exec_()

        else:
            # Show an error if the spotlight image folder could not be found
            msg.setText("The spotlight folder could not be found")
            msg.setWindowTitle("Error")
            msg.exec_()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
